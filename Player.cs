using Godot;
using System;

public class Player : KinematicBody2D {

    private AnimatedSprite sprite;
    private RayCast2D ray;
    private PlayerSound soundPlayer;

    private bool isShooting = false;

    [Export]
    public float teleportDistance = 75f;
    
    public override void _Ready() {
        sprite = GetNode<AnimatedSprite>("AnimatedSprite");
        ray = GetNode<RayCast2D>("RayCast2D");
        soundPlayer = GetNode<PlayerSound>("Sound");

        sprite.Connect("animation_finished", this, nameof(AnimationFinished));
    }

    public override void _Process(float delta) {
        if (Input.IsActionJustPressed("key_shoot")) {
            Shoot(delta);
        }
    }

    public override void _PhysicsProcess(float delta) {
        Vector2 axis = Vector2.Zero;
        if (isShooting) return;

        if (Input.IsActionJustPressed("key_teleport")) {
            MoveTeleport(delta);
            return;
        }

        if (Input.IsActionPressed("key_up")) {
            axis.y -= 1f;
        }
        if (Input.IsActionPressed("key_down")) {
            axis.y += 1f;
        }
        if (Input.IsActionPressed("key_left")) {
            axis.x -= 1f;
        }
        if (Input.IsActionPressed("key_right")) {
            axis.x += 1f;
        }

        axis = axis.Normalized();

        if (axis.y > 0f) {
            sprite.Animation = "walk_down";
        } else 
        if (axis.y < 0f) {
            sprite.Animation = "walk_up";
        } else 
        if (axis.x > 0f) {
            sprite.Animation = "walk_right";
        } else 
        if (axis.x < 0f) {
            sprite.Animation = "walk_left";
        } else {
            switch (sprite.Animation) {
                case "walk_down":   sprite.Animation = "idle_down";     break;
                case "walk_up":     sprite.Animation = "idle_up";       break;
                case "walk_right":  sprite.Animation = "idle_right";    break;
                case "walk_left":   sprite.Animation = "idle_left";     break;
            }
        }

        this.MoveAndSlide(axis * 60f);
    }

    private void Shoot(float delta) {
        Vector2 mousePos = this.GetGlobalMousePosition();
        Transform2D tr = this.Transform;
        float mouseAng = tr.origin.AngleToPoint(mousePos);
        mouseAng = 360f - (Mathf.Rad2Deg(mouseAng) + 180f);
        
        GD.Print("MousePos:  ", mousePos);
        GD.Print("PlayerPos: ", tr.origin);
        GD.Print("Angle:     ", mouseAng);

        if (mouseAng >= 315 || mouseAng < 45) { // looking right
            sprite.Animation = "shoot_right";
        } else
        if (mouseAng >= 45 && mouseAng < 135) { // looking up
            sprite.Animation = "shoot_up";
        } else 
        if (mouseAng >= 135 && mouseAng < 225) { // looking left
            sprite.Animation = "shoot_left";
        } else { // looking down ( 225 <= ang < 315 )
            sprite.Animation = "shoot_down";
        }

        isShooting = true;
    }

    private void AnimationFinished() {
        if (isShooting) {
            switch (sprite.Animation) {
                case "shoot_down":   sprite.Animation = "idle_down";     break;
                case "shoot_up":     sprite.Animation = "idle_up";       break;
                case "shoot_right":  sprite.Animation = "idle_right";    break;
                case "shoot_left":   sprite.Animation = "idle_left";     break;
            }
            isShooting = false;
        }
    }

    private void MoveTeleport(float delta) {
        Vector2 mousePos = this.GetGlobalMousePosition();
        Transform2D tr = this.Transform;
        float mouseAng = tr.origin.AngleToPoint(mousePos);
        mouseAng = 360f - (Mathf.Rad2Deg(mouseAng) + 180f);
        float mouseRad = Mathf.Deg2Rad(mouseAng);

        Vector2 newPos = new Vector2(Mathf.Cos(-mouseRad), Mathf.Sin(-mouseRad)) * teleportDistance;
        ray.CastTo = newPos;
        ray.ForceRaycastUpdate();

        if (!ray.IsColliding()) {
            tr.origin += newPos;
            this.Transform = tr;
            soundPlayer.PlaySound(PlayerSound.SOUNDS.TELEPORT);
            return;
        }

        soundPlayer.PlaySound(PlayerSound.SOUNDS.TELEPORT_ERR);
    }
}
