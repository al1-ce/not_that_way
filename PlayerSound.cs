using Godot;
using System;

public class PlayerSound : Node2D {

    public enum SOUNDS {
        TELEPORT,
        TELEPORT_ERR
    };

    private AudioStreamPlayer sndTeleport;
    private AudioStreamPlayer sndTeleportError;

    public override void _Ready() {
        sndTeleport = GetNode<AudioStreamPlayer>("AudioTeleport");
        sndTeleportError = GetNode<AudioStreamPlayer>("AudioTeleportError");
    }

    public void PlaySound(SOUNDS sound) {
        switch (sound) {
            case SOUNDS.TELEPORT:
                sndTeleport.Play();
            break;
            case SOUNDS.TELEPORT_ERR:
                sndTeleportError.Play();
            break;
        }
    }
}
