using Godot;
using System;

public class PlayerCamera : Camera2D {

    [Export]
    public NodePath playerPath = "../Player";

    private Player player;
    
    public override void _Ready() {
        player = GetNode<Player>(playerPath);
    }

    public override void _Process(float delta) {
        Transform2D tr = this.Transform;
        Transform2D pl = player.Transform;

        tr.origin = tr.origin.LinearInterpolate(pl.origin, delta * 3f);
        //tr.origin = tr.origin.Round();
        this.Transform = tr;

        this.ForceUpdateScroll();
    }
}
